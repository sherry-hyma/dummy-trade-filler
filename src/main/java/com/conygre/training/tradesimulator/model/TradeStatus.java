package com.conygre.training.tradesimulator.model;

public enum TradeStatus {
    CREATED("CREATED"),
    PROCESSING("PROCESSSING"),
    FILLED("FILLED"),
    REJECTED("REJECTED");

    private String state;

    private TradeStatus(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    } 
}
